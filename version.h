#pragma once

#ifdef __cplusplus
extern "C" {
#endif

extern const char *MetaBAT_VERSION;
extern const char *MetaBAT_VERSION_DATE;
extern const char *MetaBAT_BUILD_DATE;
extern const char *MetaBAT_BRANCH;

#ifdef __cplusplus
}
#endif
