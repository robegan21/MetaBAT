#Install instructions for supported Operating Systems

# Recent Linux Distributions with packages for MetaBAT pre-requisites:

## Docker:
-----------------
```
git clone https://bitbucket.org/berkeleylab/metabat.git
cd metabat
docker build --tag metabat .
docker run metabat runMetaBat.sh ...

```

## Build MetaBAT after all the prerequisite dependencies are met (see below)
----------------

```
git clone https://bitbucket.org/berkeleylab/metabat.git
git submodule init
git submodule update
cd metabat
mkdir -p build 
cd build
cmake -DCMAKE_INSTALL_PREFIX=$HOME/metabat ..
make
make install
cd ..
rm -rf build
```


### Install the prerequisites for Linux Ubuntu 16.04 and later (if you have root)
---------------- 
```
# install boost and a build environment
sudo apt-get update 
sudo apt-get install -y build-essential libboost-all-dev git cmake curl libncurses5-dev zlib1g-dev
```


### Install the prerequisties for Linux Fedora 20 and later (if you have root)
-------------------- 
```
#### install g++, boost and other build dependencies
sudo yum install gcc-c++ boost.x86_64 boost-devel.x86_64 zlib-devel.x86_64 libstdc++-static cmake
```


### Install boost (if you want the latest version or do *not* have root)
You can follow the directions [here](https://www.boost.org/doc/libs/1_86_0/more/getting_started/unix-variants.html)

... or just execute these commands:
```
wget https://archives.boost.io/release/1.86.0/source/boost_1_86_0.tar.gz
tar -xvzf boost_1_86_0.tar.gz
cd boost_1_86_0/
./bootstrap.sh --prefix=$HOME/boost-1.86-install
./b2 install
export BOOST_ROOT=./b2 install
```

### Install the prerequisties for MacOS X (10.14 Mojave and later) : ( using Homebrew http://brew.sh/ )
---------------------
```

# First install Xcode from the App Store (version 10.2) 
# Second install Homebrew 
# Third install llvm with openmpi and boost and cmake
brew tap homebrew/versions
brew install llvm libomp boost cmake
brew link libomp

# use the latest llvm compiler and flags
export CPPFLAGS="-I/usr/local/opt/llvm/include"
export LDFLAGS="-L/usr/local/opt/llvm/lib"
export CC=/usr/local/opt/llvm/bin/clang
export CXX=/usr/local/opt/llvm/bin/clang++

mkdir build ; cd build && cmake -DCMAKE_INSTALL_PREFIX=$HOME/metabat .. && make && make install
```


### Older distributions must build and install:
```
gcc/g++ >= 4.9 or intel >= 18.0.1 or llvm >= 8.0
boost >= 1.53
cmake >= 3.8.2
make >= 4.1
```


